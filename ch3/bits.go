package main

import "fmt"

func main() {

	// byte is an alias for uint8
	var u uint8 = 255
	fmt.Println(u, u+1, u*u)

	var i int8 = 127
	fmt.Println(i, i+1, i*i)

	var x uint8 = 1<<1 | 1<<5 // 1<<1 | 1<<5
	var y uint8 = 1<<1 | 1<<2
	fmt.Printf("%08b\n", x)
	fmt.Printf("%08b\n", y)

	for j := uint(0); j < 8; j++ {
		fmt.Printf("x\t%08b\nagainst\t%08b\nresult\t%08b\n", x, (1 << j), x&(1<<j))
		if x&(1<<j) != 0 { // membership test
			fmt.Println(">>>>> yes", j)
		}
	}
}
