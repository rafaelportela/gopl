package adjacent

// Exercise 4.5 write an in place function to eliminate adjacent duplicates in
// a []string slice.
func trim(s []string) []string {

	for i, _ := range s {
		if i+1 < len(s) && s[i] == s[i+1] {
			s = remove(s, i+1)
		}

	}
	return s
}

func remove(slice []string, i int) []string {
	copy(slice[i:], slice[i+1:])
	return slice[:len(slice)-1]
}
