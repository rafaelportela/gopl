package adjacent

import "testing"

func TestEliminateAdjacentStrings(t *testing.T) {
	s := []string{"1", "1", "2", "3", "3"}
	s = trim(s)

	if len(s) != 3 {
		t.Errorf("Failed to trim: %v\n", s)
	}

	if s[0] != "1" || s[1] != "2" || s[2] != "3" {
		t.Errorf("Failed to trim: %v\n", s)
	}
}
