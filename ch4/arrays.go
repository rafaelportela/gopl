package main

import "fmt"

func main() {

	var a [3]int
	fmt.Println(a[0])
	fmt.Println(a[len(a)-1])
	for i, v := range a {
		fmt.Printf("%d %d\n", i, v)
	}

	type Currency int
	const (
		USD Currency = iota
		EUR
		GBP
		RMB
	)
	symbol := [...]string{USD: "$", EUR: "e", GBP: "p", RMB: "i"}
	fmt.Println(RMB, symbol[RMB])
	fmt.Println(USD, symbol[USD])

	c := [2]int{1, 2}
	d := [3]int{1, 2}
	fmt.Println(c, d)
	// fmt.Println(c == d) // mismatched types [2]int and [3]int
}
