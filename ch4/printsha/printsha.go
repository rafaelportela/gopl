package main

import (
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
)

var useSha256 bool
var useSha384 bool
var useSha512 bool

func init() {
	flag.BoolVar(&useSha256, "sha256", false, "use sha256")
	flag.BoolVar(&useSha384, "sha384", false, "use sha384")
	flag.BoolVar(&useSha512, "sha512", false, "use sha512")
}

// Hash interface to hash sum bytes
type Hasher interface {
	Sum(b []byte) []byte
}

// Hash sum using sha256
type Sha256 struct{}

func (s Sha256) Sum(b []byte) []byte {
	r := sha256.Sum256(b)
	fmt.Printf("%s: %x\n", "converted", r[:])
	return r[:] // return slice, not [32]byte
}

// Hash sum using sha384
type Sha384 struct{}

func (s Sha384) Sum(b []byte) []byte {
	r := sha512.Sum384(b)
	return r[:] // return slice, not [48]byte
}

// Hash sum using sha512
type Sha512 struct{}

func (s Sha512) Sum(b []byte) []byte {
	r := sha512.Sum512(b)
	return r[:] // return slice, not [64]byte
}

func main() {

	flag.Parse()

	var hasher Hasher = new(Sha256)

	if useSha256 {
		hasher = new(Sha256)
	} else if useSha384 {
		hasher = new(Sha384)
	} else if useSha512 {
		hasher = new(Sha512)
	}

	if flag.NArg() > 0 {
		hashArg(hasher, flag.Arg(0))
	} else {
		hashStdin(hasher)
	}
}

// print hash of arg
func hashArg(h Hasher, text string) {
	fmt.Printf("%x\n", h.Sum([]byte(text)))
}

// print hash of stdin
func hashStdin(h Hasher) {
	in, _ := ioutil.ReadAll(os.Stdin)
	fmt.Printf("%x\n", h.Sum([]byte(in)))
}
