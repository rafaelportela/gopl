package rev

import "testing"

func TestReverse(t *testing.T) {

	s := []int{1, 2, 3}

	reverse(s)
	if s[0] != 3 || s[1] != 2 || s[2] != 1 {
		t.Error("Fail to reverse")
	}

}
