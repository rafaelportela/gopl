package rotate

// Exercise 4.4: Write a version of rotate that operates on a single pass.
func rotate(s []int, pos int) []int {
	i := len(s) - pos
	s = append(s[i:], s[:i]...)

	return s
}
