package rotate

import "testing"

func TestRotate(t *testing.T) {
	s := []int{1, 2, 3, 4, 5}
	//
	//
	//         4, 5, 1, 2, 3
	s = rotate(s, 2)
	if s[0] != 4 || s[1] != 5 || s[2] != 1 {
		t.Errorf("failed to rotate: %v\n", s)
	}
}
