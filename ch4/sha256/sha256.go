package main

import (
	"crypto/sha256"
	"fmt"
)

func main() {
	c1 := sha256.Sum256([]byte("x"))
	c2 := sha256.Sum256([]byte("X"))

	fmt.Printf("%x\n%x\n%T\n", c1, c2, c1) // %x print array elem in hexadecimal

	c1_1 := c1[5]
	c2_1 := c2[5]
	fmt.Printf("%08b\n%08b\n", c1_1, c2_1) // %x print array elem in hexadecimal

	fmt.Println("diff: ", countDiffBits(c1, c2))

}

// Exercise 4.1
// Count how many different bits between 2 sha256 hashes
func countDiffBits(a [32]uint8, b [32]uint8) int {

	var count int

	// for each byte
	for i := range a {
		ab := a[i] ^ b[i] // exclusive OR (XOR)

		// for each bit
		for j := uint(0); j < 8; j++ {
			if ab&(1<<j) != 0 {
				count++
			}
		}
		//fmt.Printf("%08b %d\n", ab, count)
	}
	return count
}
