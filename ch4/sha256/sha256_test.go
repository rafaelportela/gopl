package main

import (
	"crypto/sha256"
	"testing"
)

func TestCountDiffBits(t *testing.T) {
	c1 := sha256.Sum256([]byte("x"))
	c2 := sha256.Sum256([]byte("X"))

	if countDiffBits(c1, c2) != 126 {
		t.Error("The sha256 of x and X should have 125 different bits")
	}
}
